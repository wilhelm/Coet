from django.db.models import *
from django.contrib.auth.models import User

class FileModel(Model):
    # Owner of the file
    owner = ForeignKey(User)

    # Who is on the file
    # 0 : Bataillon
    # 3 : 13e Cie
    # 4 : 14e Cie
    # 31: 13e Cie, 1e Section
    # 42: 14e Cie, 2e Section
    # ...
    category = IntegerField(default=0)
    
    # Who can view the file
    # Same format
    allowed = IntegerField(default=0)

    # The file itself
    file = FileField(upload_to="uploaded_files")
