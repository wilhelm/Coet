from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, edit
from sendfile import sendfile
from app.models import *
from coet.settings import MEDIA_ROOT

class SendFileView(edit.CreateView):
    model = FileModel
    fields = ['category', 'allowed', 'file']
    template_name = 'create_file_form.html'
    success_url = "/membres/photos/envoyer?ok"
    def form_valid(self, form):
        form.instance.owner = self.request.user
        return edit.CreateView.form_valid(self, form)
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return edit.CreateView.dispatch(self, *args, **kwargs)

@login_required
def show_photo(request, photo):
    return sendfile(request, MEDIA_ROOT + '/uploaded_files/' + photo)
