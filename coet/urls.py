from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.contrib.auth.views import login, logout
from coet.views import *

from django.contrib import admin
admin.autodiscover()

#from coet.views import 

static_pages = ['cgu', 'contact', 'legal', 'eleves', 'parrain', 'chant', 'insigne', 'sponsors', 'dons', 'historique', 'activites', 'traditions', 'baptpromo', 'FOSC', 'bureau_promo_commissions', 'combat', 'CDU_13', 'sections_13', 'FMili', 'CDU_14', 'sections_14', '13_14_promo', '13_14_parrain', '13_14_chant', '13_14_insigne']

urlpatterns = patterns('',
    url(r'^$', TemplateView.as_view(template_name='index.html'), name='index'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^login$', login, name='login'),
    url(r'^logout$', logout, name='logout'),
    url(r'^infos_perso$', TemplateView.as_view(template_name='infos_perso.html') ,name='infos_perso'),###    
    url(r'^membres/photos/envoyer$', SendFileView.as_view(), name='membres/photos/envoyer'),
    url(r'^membres/photos/bataillon$', logout, name='membres/photos/bataillon'),###
    url(r'^membres/photos/compagnie(?P<compagnie>1[34])$', logout, name='membres/photos/compagnie'),###
    url(r'^membres/photos/compagnie(?P<compagnie>1[34])/section(?P<section>[1-5])$', logout, name='membres/photos/section'),###
    url(r'^photo/(?P<photo>[.a-zA-Z0-9_ -]+)$', show_photo, name='photo'),
)

for page in static_pages:
    urlpatterns += patterns('',
        url('^' + page + '$', TemplateView.as_view(template_name=page + '.html'), name=page),
    )
